#!/usr/bin/make -f
# See debhelper(7) (uncomment to enable)
# output every command that modifies files on the build system.
#export DH_VERBOSE = 1
# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
# see ENVIRONMENT in dpkg-buildflags(1)
# package maintainers to append CFLAGS
#export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic
# package maintainers to append LDFLAGS
#export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed
#export CTEST_PARALLEL_LEVEL = 1
#export CK_FORK = no

export CTEST_OUTPUT_ON_FAILURE=1

%:
	dh $@  --buildsystem=cmake --builddirectory=build

override_dh_auto_configure:
	dh_auto_configure -- $(ARGS) \
	                    -DSKIP_BUILD_RPATH=TRUE \
	                    -DBUILD_IDDAWC_TESTING=ON \
	                    -DBUILD_IDWCC=ON \
	                    -DINSTALL_HEADER=ON \
	                    --no-warn-unused-cli

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
override_dh_auto_test:
	cd test && ./cert/create-cert.sh && cp -R cert/ ../build/
	cd build && $(MAKE) test -j1
endif

override_dh_auto_build:
	doxygen doc/doxygen.cfg
	cp /usr/share/javascript/jquery/jquery.min.js tools/idwcc/webapp/js
	cp /usr/share/javascript/popper.js/umd/popper.min.js tools/idwcc/webapp/js/
	cp /usr/share/javascript/popper.js/umd/popper-utils.min.js tools/idwcc/webapp/js/
	cp /usr/share/nodejs/bootstrap/dist/js/bootstrap.min.js tools/idwcc/webapp/js/
	cp /usr/share/nodejs/bootstrap/dist/js/bootstrap.min.js.map tools/idwcc/webapp/js/
	cp /usr/share/javascript/qrcode-generator/qrcode.js tools/idwcc/webapp/js/
	cp /usr/share/nodejs/bootstrap/dist/css/bootstrap.min.css tools/idwcc/webapp/css/
	cp /usr/share/nodejs/bootstrap/dist/css/bootstrap.min.css.map tools/idwcc/webapp/css/
	cp /usr/share/fonts-fork-awesome/css/fork-awesome.css tools/idwcc/webapp/css/
	cp /usr/share/fonts-fork-awesome/css/v5-compat.css tools/idwcc/webapp/css/
	mkdir -p tools/idwcc/webapp/fonts/
	cp /usr/share/fonts/eot/fork-awesome/forkawesome-webfont.eot tools/idwcc/webapp/fonts/
	cp /usr/share/fonts/svg/fork-awesome/forkawesome-webfont.svg tools/idwcc/webapp/fonts/
	cp /usr/share/fonts/truetype/fork-awesome/forkawesome-webfont.ttf tools/idwcc/webapp/fonts/
	cp /usr/share/fonts/woff/fork-awesome/forkawesome-webfont.woff tools/idwcc/webapp/fonts/
	cp /usr/share/fonts/woff/fork-awesome/forkawesome-webfont.woff2 tools/idwcc/webapp/fonts/
	dh_auto_build --

override_dh_auto_clean:
	rm -rf doc/html test/cert/*.key test/cert/*.crt test/cert/certtool.log tools/idwcc/webapp/js/jquery* \
					tools/idwcc/webapp/js/popper* tools/idwcc/webapp/js/bootstrap* tools/idwcc/webapp/js/qrcode* \
					tools/idwcc/webapp/css/bootstrap* tools/idwcc/webapp/css/fork-awesome* tools/idwcc/webapp/css/v5-compat* \
					tools/idwcc/webapp/fonts/
	dh_auto_clean --
